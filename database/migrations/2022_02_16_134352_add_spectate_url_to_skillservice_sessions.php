<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpectateUrlToSkillserviceSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('skillservice_sessions', function (Blueprint $table) {
            //
           $table->text('spectate_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('skillservice_sessions', function (Blueprint $table) {
            //
            $table->dropColumn('spectate_url');
        });
    }
}
