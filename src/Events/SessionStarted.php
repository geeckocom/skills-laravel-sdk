<?php


namespace Geecko\Skills\Events;


use Geecko\Skills\Models\Session;
use Geecko\Skills\VO\SessionVO;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SessionStarted extends AbstractSessionEvent
{
    use Dispatchable, SerializesModels;
}
