<?php


namespace Geecko\Skills\Events;


use Geecko\Skills\Models\Session;
use Geecko\Skills\VO\SessionVO;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class AbstractSessionEvent implements SessionEventInterface
{
    use Dispatchable, SerializesModels;

    public $sessionModel;
    public $session;

    public function __construct(SessionVO $sessionVO)
    {
        $this->session = $sessionVO;
        $this->sessionModel = Session::where('uid', '=', $sessionVO->uid)->first();
        if ($this->sessionModel) {
            $this->sessionModel->saveFromVO($sessionVO);
        }
    }

}
