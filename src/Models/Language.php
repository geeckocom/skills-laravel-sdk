<?php


namespace Geecko\Skills\Models;


use Geecko\Skills\Helper;
use Geecko\Skills\VO\LanguageTaskVO;
use Geecko\Skills\VO\TaskVO;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Language extends Model
{
    protected $table = 'skillservice_langs';
    protected $guarded = [];
    public $timestamps = false;
    protected $increments = false;

    public function tasks(){
        return $this->hasMany(Task::class, 'language_id');
    }
}
