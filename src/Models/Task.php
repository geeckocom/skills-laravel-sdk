<?php


namespace Geecko\Skills\Models;


use Geecko\Skills\Helper;
use Geecko\Skills\SkillsService;
use Geecko\Skills\VO\LanguageTaskVO;
use Geecko\Skills\VO\TaskVO;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Task extends Model
{
    use SoftDeletes;

    protected $scopeLangId;


    protected $table = 'skillservice_tasks';
    protected $guarded = [];
    public $timestamps = false;
    protected $casts = [
        'structure' => 'array',
        'tags' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function setStructureAttribute($val)
    {
        if (is_array($val)) {
            $val = json_encode($val);
        }
        $this->attributes['structure'] = $val;
    }

    public function setTagsAttribute($val)
    {
        if (is_array($val)) {
            $val = json_encode($val);
        }
        $this->attributes['tags'] = $val;
    }

    public function children()
    {
        return $this->hasMany(LanguageTask::class, 'task_id');
    }
    public function setCreatedAtAttribute($val)
    {
        $this->attributes['created_at'] = Helper::parseDate($val);
    }
    public function setUpdatedAtAttribute($val)
    {
        $this->attributes['updated_at'] = Helper::parseDate($val);
    }

    public function saveFromVO(TaskVO $vo)
    {
        $data = $vo->toArray();
        $this->fill(Arr::except($data, ['tasks', 'id']));
        $this->skillservice_id = $vo->id;
        $this->save();
        $tasksIds = [];
        if($vo->type === SkillsService::TASK_TYPES_CODES[SkillsService::CODING]){
            foreach ($vo->tasks as $task) {
                $langVO = new LanguageTaskVO(array_merge($task, ['task_id' => $this->id, 'type' => $vo->type]));
                $langTask = LanguageTask::firstOrNew(['skillservice_id'=>$langVO->id,'task_id' => $this->id, 'type' => $vo->type]);
                /** @var LanguageTask $langTask */
                $langTask = $langTask->saveFromVO($langVO);
                $tasksIds[] = $langTask->id;
            }
            $this->children()->whereNotIn('id', $tasksIds)->delete();
        }

        return $this;
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $language
     * @return LanguageTask|null
     */
    public function scopeGetLangTask(\Illuminate\Database\Eloquent\Builder $query, $language){
        $lang_id = $language;
        if($language instanceof Language){
            $lang_id = $language->id;
        }
        $model =  $query->with('children')->whereHas('children', function ($q) use ($lang_id) {
            $q->where('language_id', '=', $lang_id);
        })->first();
        if(!$model){
            return null;
        }
        return $model->children->where('language_id', '=', $lang_id)->first();
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  Model  $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotUsedBy(\Illuminate\Database\Eloquent\Builder $query, $model){
        $class = get_class($model);
        $id = $model->getKey();
        $excludeIds = UsedTask::where('model_type','=',$class)->where('model_id','=',$id)->get()->pluck('task_id')->toArray();
        return $query->whereNotIn('id',$excludeIds);
    }

    public function usedTasks(){
        return $this->morphMany(UsedTask::class, 'model');
    }
}
