<?php


namespace Geecko\Skills\Models;

use Carbon\Carbon;
use Geecko\Skills\VO\SessionVO;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Geecko\Skills\Helper;
use Sentry\State\Scope;

/**
 * Class Session
 * @package Geecko\Skills\Models
 * @property Carbon|null $started_at
 * @property Carbon|null $finished_at
 * @property Carbon|null $created_at
 * @property integer|null $duration интервал времени в секундах
 * @property integer|null $score_percent
 * @property string|null $broadcast_url
 * @property string|null $spectate_url
 */
class Session extends Model
{
    protected $table = 'skillservice_sessions';
    protected $guarded = ['template_id'];
    public $timestamps = false;
    protected $casts = [
        'tasks' => 'array',
        'feedback' => 'array',
        'score' => 'array',
        'created_at' => 'datetime',
        'started_at' => 'datetime',
        'finished_at' => 'datetime',
    ];
    protected $attributes = [
        'status_id' => 0,
    ];

    protected $fillable = [
        'uid', 'url', 'model_id', 'model_type',
        'tasks', 'feedback', 'score', 'created_at',
        'updated_at', 'finished_at', 'score_percent',
        'started_at',
        'duration', 'status_id', 'redirect_uri', 'autostart', 'pass_by_tests',
        'without_feedback', 'serial',
        'broadcast_url', 'spectate_url'
    ];

    public function model()
    {
        return $this->morphTo();
    }

    public function setTasksAttribute($val)
    {
        if (is_array($val)) {
            $val = json_encode($val);
        }
        $this->attributes['tasks'] = $val;
    }

    public function setFeedbackAttribute($val)
    {
        if (is_array($val)) {
            $val = json_encode($val);
        }
        $this->attributes['feedback'] = $val;
    }

    public function setScoreAttribute($val)
    {
        if (is_array($val)) {
            $val = json_encode($val);
        }
        $this->attributes['score'] = $val;
    }

    public function setCreatedAtAttribute($val)
    {
        $this->attributes['created_at'] = Helper::parseDate($val);
    }

    public function setStartedAtAttribute($val)
    {
        $this->attributes['started_at'] = Helper::parseDate($val);
    }

    public function setFinishedAtAttribute($val)
    {
        $this->attributes['finished_at'] = Helper::parseDate($val);
    }

    public function saveFromVO(SessionVO $vo)
    {
        $data = $vo->toArray();
        $this->fill(Arr::except($data, ['candidate']));
        if ($vo->score && isset($vo->score['score']) && isset($vo->score['max'])) {
            if (intval($vo->score['max'])) {
                $this->score_percent = intval(round(intval($vo->score['score']) * 100 / intval($vo->score['max'])));
            } else {
                $this->score_percent = 0;
            }
        }

        if (!$this->created_at) {
            $this->created_at = now();
        }
        if ($this->started_at && $this->finished_at) {
            $this->duration = $this->started_at->diffInSeconds($this->finished_at);
        }
        $this->save();
        return $this;
    }


    public function getUrl(bool $darkMode = true, string $lang = 'ru')
    {
        $query = '?';
        $params = [
            'dark_mode' => $darkMode ? 'on' : 'off',
            'lang' => $lang
        ];
        foreach ($params as $key => $val) {
            $query .= $key . '=' . $val . '&';
        }
        return $this->url . $query;
    }

    public function getBroadcastUrl()
    {
        return $this->broadcast_url;
    }

    public function getSpectateUrl()
    {
        return $this->spectate_url;
    }

    public function getDuration($format = '%H:%I:%S')
    {
        if (!$this->duration) {
            return null;
        }
        return now()->subSeconds($this->duration)->diffAsCarbonInterval(now())->format($format);
    }


}
