<?php


namespace Geecko\Skills\Models;


use Geecko\Skills\VO\TemplateVO;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class UsedTask extends Model
{

    protected $table = 'skillservice_used_tasks';


    public function model()
    {
        return $this->morphTo();
    }

    public function session(){
        return $this->belongsTo(Session::class);
    }

    public function task(){
        return $this->belongsTo(Task::class);
    }

    public function langTask(){
        return $this->belongsTo(LanguageTask::class,'language_task_id');
    }
}
