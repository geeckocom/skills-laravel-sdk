<?php


namespace Geecko\Skills;


use Geecko\Skills\Models\Session;
use Geecko\Skills\VO\SessionVO;

/**
 * Class EventProcessed
 * @package Geecko\Skills\Events
 */
class EventProcessor
{

    /**
     * @var
     */
    public $sessionModel;
    /**
     * @var
     */
    public $sessionVO;
    /**
     * @var string
     */
    public $eventClass;

    /**
     * EventProcessed constructor.
     * @param string $eventClass
     */
    public function __construct(string $eventClass)
    {
        $this->eventClass = $eventClass;

    }

    /**
     * @param SessionVO $sessionVO
     */
    public function process(SessionVO $sessionVO)
    {
        $this->sessionVO = $sessionVO;
        $this->sessionModel = Session::where('uid', '=', $sessionVO->uid)->first();
        $this->fireEvent();
    }

    /**
     *
     */
    protected function fireEvent()
    {
        ($this->eventClass)::dispatch($this->sessionVO);
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return !!$this->sessionModel;
    }
}
