<?php
\Illuminate\Support\Facades\Route::namespace('Geecko\Skills\Controllers')->prefix(config('skillservice.prefix'))->group(function () {
    \Illuminate\Support\Facades\Route::post('skills-service/webhooks/session-started', 'WebhookController@sessionStarted')
        ->name('skillservice-session-started');
    \Illuminate\Support\Facades\Route::post('skills-service/webhooks/session-finished', 'WebhookController@sessionFinished')
        ->name('skillservice-session-finished');
    \Illuminate\Support\Facades\Route::post('skills-service/webhooks/session-canceled', 'WebhookController@sessionCanceled')
        ->name('skillservice-session-canceled');
    \Illuminate\Support\Facades\Route::post('skills-service/webhooks/results-received', 'WebhookController@sessionResultsReceived')
        ->name('skillservice-results-received');
    \Illuminate\Support\Facades\Route::post('skills-service/webhooks/feedback-received', 'WebhookController@sessionFeedbackReceived')
        ->name('skillservice-feedback-received');
});
