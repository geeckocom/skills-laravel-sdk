<?php


namespace Geecko\Skills;

use Carbon\Carbon;
class Helper
{

    public static function parseDate($d)
    {
        if ($d instanceof \MongoDB\BSON\UTCDateTime) {

            try {
                $date = Carbon::createFromTimestampMs($d->__toString());
                return $date;
            } catch (\Exception $e1) {
                return null;
            }
        }
        if (gettype($d) === 'integer') {

            try {
                $date = Carbon::createFromTimestampUTC($d->__toString());
                return $date;
            } catch (\Exception $e) {
                return null;
            }
        }
        try {

            $date = Carbon::parse($d);
            return $date;
        } catch (\Exception $e) {
            return null;
        }

    }
}
