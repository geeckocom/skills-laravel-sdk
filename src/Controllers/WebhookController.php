<?php


namespace Geecko\Skills\Controllers;

use Geecko\Skills\EventProcessor;
use Geecko\Skills\Events\FeedbackReceived;
use Geecko\Skills\Events\ResultsReceived;
use Geecko\Skills\Events\SessionCanceled;
use Geecko\Skills\Events\SessionEventInterface;
use Geecko\Skills\Events\SessionFinished;
use Geecko\Skills\Events\SessionStarted;
use Geecko\Skills\SkillsService;
use Geecko\Skills\VO\SessionVO;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;

/**
 * Class WebhookController
 * @package Geecko\Skills\Controllers
 * Процесс вызова вебхуков
 * Для каждого события можно указать свой URL, по которому будет осуществлен вызов.
 * Вебхук вызывается методом POST на указанный в настройках URL,
 * передавая тело запроса в формате JSON с информацией по событию. Таймаут запроса - 15 секунд.
 * Если хук вами успешно обработан, то необходимо вернуть в ответ http код 200 OK.
 * Любой другой код, отличный от 200 OK будет интерпретироваться нами как неудачный вызов,
 * после чего будут осуществлены повторные вызовы вебхука согласно таблице:
 * Номер попытки    Повторный вызов
 * 1-4    Через 30 секунд
 * 5-9    Через минуту
 * 10-14    Через 5 минут
 * 15-19    Через 10 минут
 * 20    Прекращение попыток вызова
 */
class WebhookController extends Controller
{

    protected $vo;
    protected $service;

    protected function createVO(Request $request)
    {
        $data = $request->get('data');
        $this->vo = new SessionVO(Arr::get($data, 'session'));
        if ($feedback = Arr::get($data, 'feedback')) {
            $this->vo->feedback = $feedback;
        }
    }

    /**
     * Сессия начата
     * Вызывается в момент старта сессии кандидатом.
     * @param Request $request
     * @return JsonResponse
     */
    public function sessionStarted(Request $request): JsonResponse
    {
        $this->createVO($request);
        $eventProcessor = new EventProcessor(SessionStarted::class);
        $eventProcessor->process($this->vo);
        return $this->sendResponse($eventProcessor);
    }

    /**
     * Сессия завершена
     * Вызывается в момент завершения последнего задания сессии кандидатом либо по завершении таймера последнего задания.
     * @param Request $request
     * @return JsonResponse
     */
    public function sessionFinished(Request $request): JsonResponse
    {
        $service = new SkillsService();
        $this->createVO($request);
        $session = $service->getSession($this->vo->uid);
        $this->vo->tasks = $session->tasks;
        $eventProcessor = new EventProcessor(SessionFinished::class);
        $eventProcessor->process($this->vo);
        return $this->sendResponse($eventProcessor);
    }

    /**
     * Сессия отменена
     * Вызывается в момент отмены сессии, в поле cancelled_reason_id содержится числовой код причины отмены сессии.
     * @param Request $request
     * @return JsonResponse
     */
    public function sessionCanceled(Request $request): JsonResponse
    {
        $this->createVO($request);
        $eventProcessor = new EventProcessor(SessionCanceled::class);
        $eventProcessor->process($this->vo);
        return $this->sendResponse($eventProcessor);
    }

    /**
     * Получена обратная связь от кандидата
     * Вызывается в момент отправки кандидатом обратной связи.
     * @param Request $request
     * @return JsonResponse
     */
    public function sessionFeedbackReceived(Request $request): JsonResponse
    {
        $this->createVO($request);
        $eventProcessor = new EventProcessor(FeedbackReceived::class);
        $eventProcessor->process($this->vo);
        return $this->sendResponse($eventProcessor);
    }

    /**
     * Получены результаты автопроверок для сессии
     * Поскольку итоговые автотесты связанные с выполнением кода выполняются асинхронно в очередях,
     * в момент завершения сессии возможно, что тесты по какому-то заданию еще не были запущены.
     * Данный вебхук выхызывается, когда все возможноые автопроверки для сессии выполнены.
     * @param Request $request
     * @return JsonResponse
     */
    public function sessionResultsReceived(Request $request): JsonResponse
    {
        $this->createVO($request);
        $eventProcessor = new EventProcessor(ResultsReceived::class);
        $eventProcessor->process($this->vo);
        return $this->sendResponse($eventProcessor);
    }

    /**
     * @param EventProcessor $eventProcessor
     * @return int
     */
    public function getResponseCode(EventProcessor $eventProcessor): int
    {
        return $eventProcessor->isSuccess() ? 200 : 404;
    }

    /**
     * @param EventProcessor $eventProcessor
     * @return JsonResponse
     */
    public function sendResponse(EventProcessor $eventProcessor): JsonResponse
    {
        $responseCode = $this->getResponseCode($eventProcessor);
        return response()->json(['ok' => true], $responseCode);
    }


}
