<?php


namespace Tests\Support;


use Geecko\Skills\Interfaces\SkillsSessionable;
use Geecko\Skills\Traits\SkillableTrait;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements SkillsSessionable
{
    use SkillableTrait;

    protected $guarded = [];

    public function toModelSessionableView()
    {
        return [
            "external_id" => $this->id,
            "first_name" => "Артем",
            "last_name" => "Прошин",
            "email" => $this->email,
        ];
    }
}

