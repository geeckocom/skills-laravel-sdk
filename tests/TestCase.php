<?php


namespace Tests;


use AddDurationFieldToSkillserviceSessions;
use CreateSkillserviceLangs;
use CreateSkillserviceLanguageTasksTable;
use CreateSkillserviceSessions;
use CreateSkillserviceTasksTable;
use CreateSkillserviceTemplates;
use CreateUsedTasksTable;
use Dotenv\Dotenv;
use Geecko\Skills\Console\Command\UpdateSkillServiceTasks;
use Geecko\Skills\SkillsServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Tests\Support\User;

class TestCase extends \Orchestra\Testbench\TestCase
{
    /** @var User **/
    protected $user;

    public function setUp(): void
    {
        $this->loadEnvironmentVariables();
        parent::setUp();
        $this->setUpDatabase($this->app);
        $this->user = User::first();
    }
    protected function loadEnvironmentVariables()
    {
        if (! file_exists(__DIR__.'/../.env')) {
            return;
        }

        $dotEnv = Dotenv::createImmutable(__DIR__.'/..');

        $dotEnv->load();
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    protected function getEnvironmentSetUp($app)
    {
        config()->set('database.default', 'sqlite');
        config()->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);


        config()->set('app.key', '6rE9Nz59bGRbeMATftriyQjrpF7DcOQm');

//        $this->setUpMorphMap();

    }

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    protected function setUpDatabase($app)
    {
        $app['db']->connection()->getSchemaBuilder()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->timestamps();
        });

        User::create(['name' => 'test','email'=>'some@mail']);
        $path = __DIR__.'/../database/migrations/';
        foreach(scandir($path) as $fileName){
            if(strlen($fileName)>2){
                include_once $path.$fileName;
            }
        }

        (new CreateSkillserviceTasksTable())->up();
        (new CreateSkillserviceLanguageTasksTable())->up();
        (new CreateSkillserviceTemplates())->up();
        (new CreateSkillserviceSessions())->up();
        (new CreateSkillserviceLangs())->up();
        (new AddDurationFieldToSkillserviceSessions())->up();
        (new CreateUsedTasksTable())->up();

        Artisan::call('skillservice:update:tasks');
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            SkillsServiceProvider::class,
        ];
    }

}
