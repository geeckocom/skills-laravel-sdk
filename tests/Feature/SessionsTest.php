<?php


namespace Tests\Feature;


use Geecko\Skills\Events\SessionFinished;
use Geecko\Skills\Models\Language;
use Geecko\Skills\Models\LanguageTask;
use Geecko\Skills\Models\Session;
use Geecko\Skills\Models\Task;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class SessionsTest extends TestCase
{
    /** @test */
    public function it_should_work(){
        $this->assertDatabaseCount('users',1);
        $this->assertGreaterThanOrEqual(1, Task::count());
        $this->assertGreaterThanOrEqual(1, LanguageTask::count());
    }

    /** @test */
    public function it_should_save_used_tasks(){
        $lang = Language::where('code','=','java')->first();
        $this->assertNotEmpty($lang);
        $task = Task::getLangTask($lang);
        $this->assertNotEmpty($task);
        $session = $this->user->newSession()
            ->addTask($task)
            ->skipFeedback()
            ->autostart()
            ->passByTests()
            ->create();
        $this->assertDatabaseCount('skillservice_used_tasks',1);
        $newTask = Task::notUsedBy($this->user)->getLangTask($lang);
        $this->assertNotEquals($newTask->id,$task->id);
        $this->user->newSession()
            ->addTask($newTask)
            ->skipFeedback()
            ->autostart()
            ->passByTests()
            ->create();
        $this->assertDatabaseCount('skillservice_used_tasks',2);
    }

    /** @test */
    public function it_should_process_finish_session_event()
    {
        Event::fake();
        $uid = "fcRZ02S_kn5w8ilZHeerhR6pEnpN5n";
        $session = new Session();
        $session->uid = $uid;
        $session->model()->associate($this->user);
        $session->save();
        $data = "{\"try\":11,\"data\":{\"session\":{\"uid\":\"fcRZ02S_kn5w8ilZHeerhR6pEnpN5n\",\"candidate\":null,\"status_id\":2,\"finished_at\":\"Mon, 11 Jan 2021 21:53:45 +0300\",\"created_at\":\"Sun, 10 Jan 2021 18:49:07 +0300\",\"score\":{\"max\":100,\"score\":100},\"template\":null,\"started_at\":\"Sun, 10 Jan 2021 18:49:08 +0300\",\"id\":62848}},\"called_at\":\"Mon, 11 Jan 2021 22:06:26 +0300\",\"event\":\"session:status:finished\"}";
        $request = $this->postJson(route('skillservice-session-finished'),json_decode($data, true));
        $request->assertOk();
        Event::assertDispatched(SessionFinished::class);
        $data = "{\"try\":11,\"data\":{\"session\":{\"uid\":\"fcRZ02S_kn5w8ilZHeerhR6pEnpN5n\",\"candidate\":null,\"status_id\":2,\"finished_at\":\"Mon, 11 Jan 2021 21:53:45 +0300\",\"created_at\":\"Sun, 10 Jan 2021 18:49:07 +0300\",\"score\":null,\"template\":null,\"started_at\":\"Sun, 10 Jan 2021 18:49:08 +0300\",\"id\":62848}},\"called_at\":\"Mon, 11 Jan 2021 22:06:26 +0300\",\"event\":\"session:status:finished\"}";
        $request = $this->postJson(route('skillservice-session-finished'),json_decode($data, true));
        $request->assertOk();
        Event::assertDispatched(SessionFinished::class);
    }

}
